package com.example.controller;

import com.example.bean.LoginBean;

import java.io.IOException;

public class ServletController extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        //tep 1
        String username  = request.getParameter("username");
        String password = request.getParameter("password");
        //step2
        LoginBean loginBean = new LoginBean();
        loginBean.setUsername(username);
        loginBean.setPassword(password);

        boolean status = loginBean.checkLogin();
        //send status (Attribute)
        request.setAttribute("bean",loginBean);
        if (status)
        {
            response.sendRedirect("success.jsp");
        }else {
            response.sendRedirect("fail.jsp");
        }

    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }
}
